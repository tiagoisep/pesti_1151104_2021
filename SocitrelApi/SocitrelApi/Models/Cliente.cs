﻿namespace SocitrelAPI.Models
{
    public class Cliente
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsComplete { get; set; }
    }
}