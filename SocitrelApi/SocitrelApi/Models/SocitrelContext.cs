﻿

using Microsoft.EntityFrameworkCore;

namespace SocitrelAPI.Models
{
    public class SocitrelContext : DbContext
    {
        public SocitrelContext(DbContextOptions<SocitrelContext> options)
            : base(options)
        {
        }

        public DbSet<Cliente> Clientes { get; set; }
    }
}