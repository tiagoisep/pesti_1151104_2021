import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Cliente } from './MODEL/cliente';


@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const clientes = [
      { id: 1, nomeCliente: 'SINFLEX' },
      { id: 2, nomeCliente: 'ATHANOR' },
      { id: 3, nomeCliente: 'BAUMANN' },
      { id: 4, nomeCliente: 'CELBI' },
      { id: 5, nomeCliente: 'BIANCHINI' },
      { id: 6, nomeCliente: 'BOSCH' },
      { id: 7, nomeCliente: 'MECALUX' },
      { id: 8, nomeCliente: 'RPK' },
      { id: 9, nomeCliente: 'CABELT' },
      { id: 10, nomeCliente: 'NAVIGATOR' }
    ];
    return {clientes};
  }

  // Overrides the genId method to ensure that a hero always has an id.
  // If the clientes array is empty,
  // the method below returns the initial number (11).
  // if the clientes array is not empty, the method below returns the highest
  // hero id + 1.
  genId(clientes: Cliente[]): number {
    return clientes.length > 0 ? Math.max(...clientes.map(hero => hero.id)) + 1 : 11;
  }
}