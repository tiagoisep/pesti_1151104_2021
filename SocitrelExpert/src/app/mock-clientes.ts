import { Cliente } from "./MODEL/cliente";


export const CLIENTES: Cliente[] = [
  { id: 1, nomeCliente: 'SINFLEX' },
  { id: 2, nomeCliente: 'ATHANOR' },
  { id: 3, nomeCliente: 'BAUMANN' },
  { id: 4, nomeCliente: 'CELBI' },
  { id: 5, nomeCliente: 'BIANCHINI' },
  { id: 6, nomeCliente: 'BOSCH' },
  { id: 7, nomeCliente: 'MECALUX' },
  { id: 8, nomeCliente: 'RPK' },
  { id: 9, nomeCliente: 'CABELT' },
  { id: 10, nomeCliente: 'NAVIGATOR' }
];