import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClientesComponent } from './COMPONENTS/clientes/clientes.component';
import { DetalhesClientesComponent } from './COMPONENTS/detalhes-clientes/detalhes-clientes.component';
import { NotificacoesComponent } from './COMPONENTS/notificacoes/notificacoes.component';
import { DashboardComponent } from './COMPONENTS/dashboard/dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';
import { ClienteSearchComponent } from './COMPONENTS/cliente-search/cliente-search.component';

@NgModule({
  declarations: [
    AppComponent,
    ClientesComponent,
    DetalhesClientesComponent,
    NotificacoesComponent,
    DashboardComponent,
    ClienteSearchComponent
  ],
  imports: [
    HttpClientModule,
    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    ),
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
