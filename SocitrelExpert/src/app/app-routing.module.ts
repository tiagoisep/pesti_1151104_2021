import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientesComponent } from './COMPONENTS/clientes/clientes.component';
import { DashboardComponent } from './COMPONENTS/dashboard/dashboard.component';
import { DetalhesClientesComponent } from './COMPONENTS/detalhes-clientes/detalhes-clientes.component';



const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  
  { path: 'clientes', component: ClientesComponent },
  { path: 'detalhesClientes/:id', component: DetalhesClientesComponent }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }