import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/MODEL/cliente';
import { ClienteService } from 'src/app/SERVICES/cliente.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {
  clientes: Cliente[] = [];

  constructor(private clienteService: ClienteService) { }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.clienteService.getClientes()
      .subscribe(clientes => this.clientes = clientes.slice(1, 5));
  }
}