
import { Component, OnInit, Input } from '@angular/core';
import { Cliente } from '../../MODEL/cliente';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ClienteService } from 'src/app/SERVICES/cliente.service';

@Component({
  selector: 'app-detalhes-clientes',
  templateUrl: './detalhes-clientes.component.html',
  styleUrls: ['./detalhes-clientes.component.css']
})
export class DetalhesClientesComponent implements OnInit {

  @Input() cliente?: Cliente;

  constructor(
    private route: ActivatedRoute,
    private clienteService: ClienteService,
    private location: Location

  ) { }

  ngOnInit(): void {
    this.getCliente();
  }

  getCliente(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.clienteService.getCliente(id)
      .subscribe(cliente => this.cliente = cliente);
  }
  goBack(): void {
    this.location.back();
  }
  save(): void {
    if (this.cliente) {
      this.clienteService.updateCliente(this.cliente)
        .subscribe(() => this.goBack());
    }
  }

}
