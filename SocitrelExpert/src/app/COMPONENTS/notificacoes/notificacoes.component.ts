import { Component, OnInit } from '@angular/core';
import { NotificacoesService } from 'src/app/SERVICES/notificacoes.service';


@Component({
  selector: 'app-notificacoes',
  templateUrl: './notificacoes.component.html',
  styleUrls: ['./notificacoes.component.css']
})
export class NotificacoesComponent implements OnInit {

  constructor(public notificacoesService: NotificacoesService) { }

  ngOnInit(): void {
  }

}
