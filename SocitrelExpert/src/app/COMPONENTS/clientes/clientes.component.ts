import { Component, OnInit } from '@angular/core';
import { NotificacoesService } from 'src/app/SERVICES/notificacoes.service';
import { CLIENTES } from '../../mock-clientes';
import { Cliente } from '../../MODEL/cliente';
import { ClienteService } from '../../SERVICES/cliente.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})

export class ClientesComponent implements OnInit {
  clientes: Cliente[] = [];

  constructor(private clienteService: ClienteService) { }

  ngOnInit() {
    this.getClientes();
  }

  getClientes(): void {
    this.clienteService.getClientes()
    .subscribe(clientes => this.clientes = clientes);
  }

  add(nomeCliente: string): void {
    nomeCliente = nomeCliente.trim();
    if (!nomeCliente) { return; }
    this.clienteService.addCliente({ nomeCliente } as Cliente)
      .subscribe(cliente => {
        this.clientes.push(cliente);
      });
  }

  delete(cliente: Cliente): void {
    this.clientes = this.clientes.filter(c => c !== cliente);
    this.clienteService.deleteCliente(cliente.id).subscribe();
  }

}

