import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class NotificacoesService {
  notificacoes: string[] = [];

  add(notificacao: string) {
    this.notificacoes.push(notificacao);
  }

  clear() {
    this.notificacoes = [];
  }
}