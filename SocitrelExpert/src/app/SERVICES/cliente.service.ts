import { Injectable } from '@angular/core';
import { CLIENTES } from '../mock-clientes';
import { Cliente } from '../MODEL/cliente';
import { Observable, of } from 'rxjs';
import { NotificacoesService } from './notificacoes.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  private clientesUrl = 'api/clientes';  // URL to web api


  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private notificacoesService: NotificacoesService) { }

  /**
      getClientes(): Observable<Cliente[]> {
      const clientes = of(CLIENTES);
      this.notificacoesService.add('Notificação: aqui estão os seu clientes :)');
      return clientes;
    }
    */

/** GET heroes from the server */
getClientes(): Observable<Cliente[]> {
  return this.http.get<Cliente[]>(this.clientesUrl)
    .pipe(
      tap(_ => this.log('fetched clientes')),
      catchError(this.handleError<Cliente[]>('getClientes', []))
    );
}

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

/** GET cliente by id. Will 404 if id not found */
getCliente(id: number): Observable<Cliente> {
  const url = `${this.clientesUrl}/${id}`;
  return this.http.get<Cliente>(url).pipe(
    tap(_ => this.log(`fetched cliente id=${id}`)),
    catchError(this.handleError<Cliente>(`getCliente id=${id}`))
  );
}
  /*getCliente(id: number): Observable<Cliente> {
    // For now, assume that a cliente with the specified `id` always exists.
    // Error handling will be added in the next step of the tutorial.
    const cliente = CLIENTES.find(c => c.id === id)!;
    this.notificacoesService.add(`Notificação: Aqui está o seu cliente id=${id}`);
    return of(cliente);
  }*/

  /** PUT: update the cliente on the server */
updateCliente(cliente: Cliente): Observable<any> {
  return this.http.put(this.clientesUrl, cliente, this.httpOptions).pipe(
    tap(_ => this.log(`updated cliente id=${cliente.id}`)),
    catchError(this.handleError<any>('updateClient'))
  );
}
/** POST: add a new cliente to the server */
addCliente(cliente: Cliente): Observable<Cliente> {
  return this.http.post<Cliente>(this.clientesUrl, cliente, this.httpOptions).pipe(
    tap((newCliente: Cliente) => this.log(`added cliente w/ id=${newCliente.id}`)),
    catchError(this.handleError<Cliente>('addCliente'))
  );
}

/** DELETE: delete the hero from the server */
deleteCliente(id: number): Observable<Cliente> {
  const url = `${this.clientesUrl}/${id}`;

  return this.http.delete<Cliente>(url, this.httpOptions).pipe(
    tap(_ => this.log(`deleted cliente id=${id}`)),
    catchError(this.handleError<Cliente>('deleteCliente'))
  );
}



/* GET heroes whose name contains search term */
searchClientes(term: string): Observable<Cliente[]> {
  if (!term.trim()) {
    // if not search term, return empty hero array.
    return of([]);
  }
  return this.http.get<Cliente[]>(`${this.clientesUrl}/?name=${term}`).pipe(
    tap(x => x.length ?
       this.log(`found clientes matching "${term}"`) :
       this.log(`no clientes matching "${term}"`)),
    catchError(this.handleError<Cliente[]>('searchClientes', []))
  );
}


  /** Log a HeroService message with the MessageService */
  private log(notificacao: string) {
    this.notificacoesService.add(`NotificacaoService: ${notificacao}`);
  }

}
